import {Component, AfterViewInit, ViewChild, ViewContainerRef, OnDestroy} from "@angular/core";
import {Http} from "@angular/http";
var Rx = require('@reactivex/rxjs'); //import { Observable } from 'rxjs';

@Component({
    selector: "autocomplete",
    template: require("./autocomplete.component.html"),
    styles: [require("./autocomplete.component.scss").toString()]
})
export class AutocompleteComponent implements AfterViewInit, OnDestroy {
    public inputListener:any;
    public selectListener:any;
    public results:any[] = [];
    public resLength:number;
    public selected:string;
    public inputValue:string;
    public theMovie:any;
    public error:string;
    public unsubscriber1:any;
    public unsubscriber2:any;
    @ViewChild('inputvcr', {read: ViewContainerRef}) private inputVcr:ViewContainerRef;
    @ViewChild('selectvcr', {read: ViewContainerRef}) private selectVcr:ViewContainerRef;

    constructor(private http:Http) {
    }

    ngAfterViewInit() {
        let el = this.inputVcr.element.nativeElement;
        let sel = this.selectVcr.element.nativeElement;

        this.selectListener = Rx.Observable.fromEvent(sel, 'keyup'); //Without Rx if import used!
        this.unsubscriber1 = this.selectListener
            .subscribe((data) => {
                this.inputValue = this.selected;
                if (data.keyCode === 13) {
                    this.select(this.selected);
                }
                if (data.keyCode === 40 && this.selected === this.results[this.results.length - 1].Title) {
                    this.selected = this.results[0].Title;
                }
                if (data.keyCode === 38 && this.selected === this.results[0].Title) {
                    el.focus();
                }
                if (data.keyCode !== 38 && data.keyCode !== 40) { //Down and Up for Edge
                    el.focus();
                }
            });

        this.inputListener = Rx.Observable.fromEvent(el, 'keyup'); //Without Rx if import used!
        this.unsubscriber2 = this.inputListener
            .throttleTime(150)
            .filter(ev => {
                return ev.target.value.length > 3;
            })
            .switchMap((event) => {
                console.log(event);
                if (event.keyCode === 40) { //Down and Up for Edge
                    sel.focus();
                }
                if (event.keyCode === 13) {
                    this.select(this.selected);
                }
                return this.http.get(`http://www.omdbapi.com/?s=${event.target.value}&r=json`);
            })
            .subscribe((data)=> {
                let result = JSON.parse(data._body);
                if (result.Search) {
                    this.error = '';
                    this.results = result.Search;
                    this.resLength = result.Search.length === 1 ? 2 : result.Search.length;
                    this.selected = result.Search[0].Title;
                } else {
                    this.resLength = 0;
                    this.results.length = 0;
                    this.error = result.Error;
                }
            });
    }

    select(val) {
        this.results.length = 0;
        this.inputValue = val;
        this.selected = '';
        this.resLength = 0;
        this.http.get(`http://www.omdbapi.com/?t=${val}&y=&plot=short&r=json`)
            .subscribe((data:any) => {
                this.theMovie = JSON.parse(data._body);
            })
    }

    ngOnDestroy() {
        this.unsubscriber1._subscriptions[0]._unsubscribe();
        this.unsubscriber2._subscriptions[0]._unsubscribe();
    }
}
