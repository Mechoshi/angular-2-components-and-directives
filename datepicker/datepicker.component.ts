import {
    Component,
    EventEmitter,
    OnInit,
    Output
} from '@angular/core';
import * as moment from 'moment/moment'; //npm install moment --save

/**
 * Create a Date type to enable the
 * safe typing of day objects.
 */
class Day {
    public dayOfWeek:number;
    public dayOfWeekName:string;
    public dayOfMonth:number;
    public month:number;
    public monthName:string;
    public year:number;
    public timestamp:number;
}

/**
 * Create a Month type to enable the
 * safe typing of month objects.
 */
class Month {
    public name:string;
    public number:number;
    public year:number;
    public structure:Day[][];
    public weekDaysNames:string[];

    constructor() {
        let array = new Array(6);
        for (let i = 0; i < array.length; i++) {
            let subArray = new Array(7);
            for (let j = 0; j < subArray.length; j++) {
                subArray[j] = null;
            }
            array[i] = subArray;
        }
        this.structure = array;
        this.weekDaysNames = new Array(7);
    }
}

@Component({
    selector: 'datepicker',
    template: require('./datepicker.component.html'),
    styles: [require('./datepicker.component.scss').toString()]
})
export class DatepickerComponent implements OnInit {
    /**
     * Object representing the current day.
     */
    public today:Day;
    /**
     * Currently selected day.
     * Today as default (on component creation).
     */
    public selectedDay:Day;
    /**
     * Holds the days of the currently
     * selected month
     */
    public selectedMonth:Month;
    private DIRECTION_NEXT:string = 'next';
    private DIRECTION_PREV:string = 'prev';
    private PERIOD_MONTH:string = 'month';
    private PERIOD_YEAR:string = 'year';
    public flagNext:boolean = false;
    public flagPrev:boolean = false;
    @Output() public datepick:EventEmitter;

    constructor() {
        this.today = new Day();
        this.selectedDay = new Day();
        this.selectedMonth = new Month();
        /**
         * Get the user's browser language
         * setting and set moment's locale
         * to the corresponding language.
         * @type {string}
         */
        const locale = window.navigator.userLanguage || window.navigator.language;
        moment.locale(locale);
        this.datepick = new EventEmitter();
    }

    /**
     * Initialise the current day,
     * set selected day to today as default
     * and set the selected month to the
     * month corresponding to today's date.
     */
    ngOnInit() {
        let today = moment();
        /**
         * Initialise today.
         */
        this.setDay(this.today, today);
        /**
         * Set today as default selected day.
         */
        this.assignDay(this.selectedDay, this.today);
        /**
         * Set the selected month to the month
         * corresponding to today's month.
         */
        this.createMonthObject(this.selectedDay.year, this.selectedDay.month, this.selectedMonth);
    }

    /**
     * Set the properties of a day,
     * given a moment object.
     * @param date
     */
    private setDay(newDay:Day, date:moment.Moment):Day {
        newDay.dayOfWeek = parseInt(date.format('E'));
        newDay.dayOfWeekName = date.format('dd');
        newDay.dayOfMonth = date.date();
        newDay.month = date.month(); //Months in JS are numbered from 0 to 11.
        newDay.monthName = date.format('MMMM');
        newDay.year = date.year();
        newDay.timestamp = moment({
            year: newDay.year,
            month: newDay.month,
            day: newDay.dayOfMonth
        }).valueOf();
        return newDay;
    }

    /**
     * Assign day to other day by creating a
     * new object, not a reference.
     */
    private assignDay(target:Day, source:Day):void {
        target.dayOfWeek = source.dayOfWeek;
        target.dayOfWeekName = source.dayOfWeekName;
        target.dayOfMonth = source.dayOfMonth;
        target.month = source.month;
        target.monthName = source.monthName;
        target.year = source.year;
        target.timestamp = source.timestamp;
    }

    /**
     * Based on a selected day this method sets the
     * selected month object with the corresponding
     * month's days.
     * @param selectedDay
     * @param selectedMonth
     */
    private createMonthObject(year:number, monthNumber:number, month:Month):Month {
        let monthMoment = moment({
            year: year,
            month: monthNumber,
            day: 1
        });
        month.year = year;
        month.number = monthNumber;
        month.name = monthMoment.format('MMMM');
        const daysInMonth = monthMoment.daysInMonth();
        let week = 0;
        for (let i = 1; i <= daysInMonth; i++) {
            const dayOfMonth = this.setDay(new Day(), monthMoment);
            month.structure[week][dayOfMonth.dayOfWeek - 1] = dayOfMonth;
            /**
             * Set the weekDaysNames property to hold the name of the
             * days of the week corresponding to the user's browser
             * language settings.
             */
            if (month.weekDaysNames[dayOfMonth.dayOfWeek - 1] === undefined) {
                month.weekDaysNames[dayOfMonth.dayOfWeek - 1] = dayOfMonth.dayOfWeekName;
            }
            /**
             * If the current day is the last day of the current week
             * increment the week counter.
             */
            if (dayOfMonth.dayOfWeek === 7) {
                week++;
            }
            /**
             * Move the object, holding the current moment,
             * with one day.
             */
            monthMoment.add(1, 'days');
        }
        return month;
    }

    /**
     * Pick a date from the view.
     * @param date
     */
    public pickDate(day):void {
        if (day) {
            this.selectedDay = day;
            console.log(this.selectedDay);
            this.datepick.emit(this.selectedDay);
        }
    }

    /**
     * Method invoked in the view in order to
     * switch months.
     * @param direction
     * @param period
     */
    public switchPeriod(direction:string, period:string):void {
        if ((direction !== this.DIRECTION_NEXT && direction !== this.DIRECTION_PREV)
            || (period !== this.PERIOD_MONTH && period !== this.PERIOD_YEAR)) {
            return;
        }

        this.flagNext = false;
        this.flagPrev = false;

        let month;
        let year;
        if (direction === this.DIRECTION_NEXT) {
            if (period === this.PERIOD_MONTH) {
                if (this.selectedMonth.number === 11) {
                    month = 0;
                    year = this.selectedMonth.year + 1;
                } else {
                    month = this.selectedMonth.number + 1;
                    year = this.selectedMonth.year;
                }
            } else if (period === this.PERIOD_YEAR) {
                month = this.selectedMonth.number;
                year = this.selectedMonth.year + 1;
            }
        } else if (direction === this.DIRECTION_PREV) {
            if (period === this.PERIOD_MONTH) {
                if (this.selectedMonth.number === 0) {
                    month = 11;
                    year = this.selectedMonth.year - 1;
                } else {
                    month = this.selectedMonth.number - 1;
                    year = this.selectedMonth.year;
                }
            } else if (period === this.PERIOD_YEAR) {
                month = this.selectedMonth.number;
                year = this.selectedMonth.year - 1;
            }
        }
        this.selectedMonth = null;
        this.selectedMonth = this.createMonthObject(year, month, new Month());
        setTimeout(()=> {
            if (direction === this.DIRECTION_NEXT) {
                this.flagNext = true;
            } else if (direction === this.DIRECTION_PREV) {
                this.flagPrev = true;
            }
        }, 0);
    }
}
